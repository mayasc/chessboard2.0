import _ from 'lodash';
import constants from '../src/constants';
import moveCalculations from '../src/moveCalculations';

function compareArraysOfObjects(a,b) {
    if (a.length !== b.length) {
        return false;
    }
    var c = a.filter(function(value) {
        return !_.isUndefined(_.find(b, value));
    });

    return  c.length === a.length ? true : false;
}

describe('moveCalculations', function () {

    describe('getPotentialMoves', function () {

        it('Should not allow moves overriding a soldier of same turn', function () {
            var expectedResult = [
                {row: 6, col: 6},
                {row: 6, col: 8}
            ];
            var placement = constants.INITIAL_PLACEMENT;

            var result = moveCalculations.getPotentialMoves(8, 7, 5, constants.WHITE, placement);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });
    });

    describe('getKingValidSteps', function () {

        it('should return the correct valid steps of the king in the given position', function () {
            var expectedResult = [
                {row: 3, col: 5},
                {row: 3, col: 6},
                {row: 3, col: 7},
                {row: 4, col: 5},
                {row: 4, col: 7},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7}
            ];
            // row: 4, col: 6, pieceId: 1
            expect(moveCalculations.getPotentialMoves(4, 6, 1, constants.WHITE, [])).toEqual(expectedResult); //TODO turn 4,6,1 to an object to make the parameter clear to read
        });

        it('should work the same for black king (pieceId 7)', function () {
            var expectedResult = [
                {row: 3, col: 5},
                {row: 3, col: 6},
                {row: 3, col: 7},
                {row: 4, col: 5},
                {row: 4, col: 7},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7}
            ];
            // row: 4, col: 6, pieceId: 7
            expect(moveCalculations.getPotentialMoves(4, 6, 7, constants.BLACK, [])).toEqual(expectedResult);
        });
    });

    describe('getQueenValidSteps', function () {

        it('should return the correct valid steps of the queen in the given position', function () {
            var expectedResult = [
                {row: 5, col: 1},
                {row: 5, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7},
                {row: 5, col: 8},
                {row: 1, col: 4},
                {row: 2, col: 4},
                {row: 3, col: 4},
                {row: 4, col: 4},
                {row: 6, col: 4},
                {row: 7, col: 4},
                {row: 8, col: 4},
                {row: 2, col: 1},
                {row: 3, col: 2},
                {row: 4, col: 3},
                {row: 1, col: 8},
                {row: 2, col: 7},
                {row: 3, col: 6},
                {row: 4, col: 5},
                {row: 7, col: 2},
                {row: 6, col: 5},
                {row: 7, col: 6},
                {row: 8, col: 7},
                {row: 6, col: 3},
                {row: 8, col: 1}
            ];
            // row: 5, col: 4, pieceId: 2
            var result = moveCalculations.getPotentialMoves(5, 4, 2, constants.WHITE, []);

            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });

        it('should work the same for black queen (pieceId 8)', function () {
            var expectedResult = [
                {row: 5, col: 1},
                {row: 5, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7},
                {row: 5, col: 8},
                {row: 1, col: 4},
                {row: 2, col: 4},
                {row: 3, col: 4},
                {row: 4, col: 4},
                {row: 6, col: 4},
                {row: 7, col: 4},
                {row: 8, col: 4},
                {row: 2, col: 1},
                {row: 3, col: 2},
                {row: 4, col: 3},
                {row: 1, col: 8},
                {row: 2, col: 7},
                {row: 3, col: 6},
                {row: 4, col: 5},
                {row: 7, col: 2},
                {row: 6, col: 5},
                {row: 7, col: 6},
                {row: 8, col: 7},
                {row: 6, col: 3},
                {row: 8, col: 1}
            ];
            // row: 5, col: 4, pieceId: 8

            var result = moveCalculations.getPotentialMoves(5, 4, 2, constants.BLACK, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });
    });

    describe('getRookValidSteps', function () {

        it('should return the correct valid steps of the rook in the given position', function () {
            var expectedResult = [
                {row: 5, col: 1},
                {row: 5, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7},
                {row: 5, col: 8},
                {row: 1, col: 4},
                {row: 2, col: 4},
                {row: 3, col: 4},
                {row: 4, col: 4},
                {row: 6, col: 4},
                {row: 7, col: 4},
                {row: 8, col: 4}
            ];
            // row: 5, col: 4, pieceId: 3

            var result = moveCalculations.getPotentialMoves(5, 4, 3, constants.WHITE, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });

        it('should work the same for black rook (pieceId 9)', function () {
            var expectedResult = [
                {row: 5, col: 1},
                {row: 5, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 5, col: 6},
                {row: 5, col: 7},
                {row: 5, col: 8},
                {row: 1, col: 4},
                {row: 2, col: 4},
                {row: 3, col: 4},
                {row: 4, col: 4},
                {row: 6, col: 4},
                {row: 7, col: 4},
                {row: 8, col: 4}
            ];
            var result = moveCalculations.getPotentialMoves(5, 4, 9, constants.BLACK, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });
    });

    describe('getBishopValidSteps', function () {

        it('should return the correct valid steps of the bishop in the given position', function () {
            var expectedResult = [
                {row: 1, col: 1},
                {row: 2, col: 2},
                {row: 3, col: 3},
                {row: 1, col: 7},
                {row: 2, col: 6},
                {row: 3, col: 5},
                {row: 7, col: 1},
                {row: 6, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 6, col: 6},
                {row: 7, col: 7},
                {row: 8, col: 8}
            ];
            // row: 4, col: 4, pieceId: 4
            var result = moveCalculations.getPotentialMoves(4, 4, 4, constants.WHITE, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });

        it('should work the same for black bishop (pieceId 10)', function () {
            var expectedResult = [
                {row: 1, col: 1},
                {row: 2, col: 2},
                {row: 3, col: 3},
                {row: 1, col: 7},
                {row: 2, col: 6},
                {row: 3, col: 5},
                {row: 7, col: 1},
                {row: 6, col: 2},
                {row: 5, col: 3},
                {row: 5, col: 5},
                {row: 6, col: 6},
                {row: 7, col: 7},
                {row: 8, col: 8}
            ];
            // row: 4, col: 4, pieceId: 10
            var result = moveCalculations.getPotentialMoves(4, 4, 10, constants.BLACK, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });
    });

    describe('getKnightValidSteps', function () {

        it('should return the correct valid steps of the knight in the given position', function () {
            var expectedResult = [
                {row: 4, col: 2},
                {row: 3, col: 3},
                {row: 4, col: 6},
                {row: 3, col: 5},
                {row: 6, col: 2},
                {row: 7, col: 3},
                {row: 6, col: 6},
                {row: 7, col: 5}
            ];
            // row: 5, col: 4, pieceId: 5
            var result = moveCalculations.getPotentialMoves(5, 4, 5, constants.WHITE, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });

        it('should work the same for black knight (pieceId 11)', function () {
            var expectedResult = [
                {row: 4, col: 2},
                {row: 3, col: 3},
                {row: 4, col: 6},
                {row: 3, col: 5},
                {row: 6, col: 2},
                {row: 7, col: 3},
                {row: 6, col: 6},
                {row: 7, col: 5}
            ];
            // row: 5, col: 4, pieceId: 11
            var result = moveCalculations.getPotentialMoves(5, 4, 11, constants.BLACK, []);
            expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
        });
    });

    describe('getPawnValidSteps', function () {

        describe('white pawn', function () {

            it('should return the correct steps of the pawn in the given position when board in front is vacant and the pawn ' +
                'is in its original position', function () {
                var expectedResult = [
                    {row: 5, col: 4},
                    {row: 6, col: 4}
                ];
                // row: 7, col: 4, pieceId: 6, turn: WHITE, placement: []
                var result = moveCalculations.getPotentialMoves(7, 4, 6, constants.WHITE, []);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });

            it('should return the correct step of the pawn in the given position when board in front is vacant and the pawn ' +
                'is not in its original position', function () {
                var expectedResult = [
                    {row: 5, col: 4}
                ];
                // row: 6, col: 4, pieceId: 6, turn: WHITE, placement: []
                var result = moveCalculations.getPotentialMoves(6, 4, 6, constants.WHITE, []);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });

            it('should return the correct valid steps of the pawn in the given position when board in front is not vacant', function () {
                var expectedResult = [
                    {row: 4, col: 5},
                    {row: 4, col: 4},
                    {row: 4, col: 3}
                ];
                var placement = [
                    {row: 4, col: 5, pieceId: constants.PIECES.BLACK_PAWN.id},
                    {row: 4, col: 3, pieceId: constants.PIECES.BLACK_PAWN.id}
                ];
                // row: 5, col: 4, pieceId: 12, turn: WHITE
                var result = moveCalculations.getPotentialMoves(5, 4, 12, constants.WHITE, placement);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });
        });

        describe('black pawn', function () {

            it('should return the correct steps of the pawn in the given position when board in front is vacant and the pawn ' +
                'is in its original position', function () {
                var expectedResult = [
                    {row: 3, col: 4},
                    {row: 4, col: 4}
                ];
                var result = moveCalculations.getPotentialMoves(2, 4, 6, constants.BLACK, []);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });

            it('should return the correct step of the pawn in the given position when board in front is vacant and the pawn ' +
                'is not in its original position', function () {
                var expectedResult = [
                    {row: 4, col: 4}
                ];
                // row: 6, col: 4, pieceId: 6, turn: WHITE, placement: []
                var result = moveCalculations.getPotentialMoves(3, 4, 6, constants.BLACK, []);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });

            it('should return the correct valid steps of the pawn in the given position when board in front is not vacant', function () {
                var expectedResult = [
                    {row: 6, col: 3},
                    {row: 6, col: 4},
                    {row: 6, col: 5}
                ];
                var placement = [
                    {row: 6, col: 3, pieceId: constants.PIECES.WHITE_PAWN.id},
                    {row: 6, col: 5, pieceId: constants.PIECES.WHITE_PAWN.id}
                ];

                var result = moveCalculations.getPotentialMoves(5, 4, 12, constants.BLACK, placement);
                expect(compareArraysOfObjects(expectedResult, result)).toBeTruthy();
            });
        });
    });
});
