import React from 'react';
import constants from '../constants';
import _ from 'lodash';
import Cell from './Cell';
import moveCalculations from '../moveCalculations'

export default React.createClass({
    displayName: 'Board',

    getInitialState: function () {
        return {
            piecesPlacement: constants.INITIAL_PLACEMENT,
            turn: constants.WHITE,
            selectedPiece: null,
            potentialMoves: null
        }
    },

    selectPiece: function (row, col, pieceId) {
        this.setState({
            potentialMoves: moveCalculations.getPotentialMoves(row, col, pieceId, this.state.turn, this.state.piecesPlacement),
            selectedPiece: {row: row, col: col, pieceId: pieceId}
        });
    },

    selectMove: function (row, col) {
        var newPlacement = _.clone(this.state.piecesPlacement);
        _.remove(newPlacement, this.state.selectedPiece);
        _.remove(newPlacement, {row: row, col: col});
        newPlacement.push({row: row, col: col, pieceId: this.state.selectedPiece.pieceId});

        this.setState({
            piecesPlacement: newPlacement,
            selectedPiece: null,
            potentialMoves: null,
            turn: this.state.turn === constants.WHITE ? constants.BLACK : constants.WHITE
        });
    },

    createBoardCells: function () {
        return _.map(constants.TABLE_ROW, function (rowNum) {
            return _.map(constants.TABLE_ROW, function (colNum) {
                var pieceId = _.find(this.state.piecesPlacement, {row: rowNum, col: colNum});
                pieceId = _.get(pieceId, 'pieceId', null); //TODO chain this
                var isPotentialMove = _.find(this.state.potentialMoves, {row: rowNum, col: colNum}) ? true : false;

                return (<Cell rowNum={rowNum} colNum={colNum}
                        pieceId={pieceId}
                        turn={this.state.turn}
                        isPotentialMove={isPotentialMove}
                        selectPiece={this.selectPiece}
                        selectMove={this.selectMove} />);
            }.bind(this));
        }.bind(this));
    },

    render: function () {
        var tableCells = this.createBoardCells();
        var whosTurnText = (this.state.turn === constants.WHITE ? 'White\'s' : 'Black\'s') + ' turn';

        return (<div className="board">
                    <div className="whos-turn"> {whosTurnText} </div>
                    {tableCells}
                </div>)
    }
});