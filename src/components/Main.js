import React from 'react';
import ReactDOM from 'react-dom';
import Board from './Board';

export default React.createClass({
    displayName: 'Main',
    render: function () {
        return (
        <div className="main">
            <h1> Chess Board </h1>

            <Board/>
        </div>
        );
    }
});
