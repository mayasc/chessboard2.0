import React from 'react';
import constants from '../constants';
import _ from 'lodash';

function getColorClass (row, col) {
    if (row === 0 || col === 0) {
        return '';
    }
    var rowEven = row % 2 === 0;
    var colEven = col % 2 === 0;
    if (rowEven && colEven || (!rowEven && !colEven)) {
        return 'black';
    }
    return 'white';
}

function isSelectableCell(pieceId, turn) {
    return ((_.includes(constants.WHITE_PIECE_IDS, pieceId) && turn === constants.WHITE) ||
        (_.includes(constants.BLACK_PIECE_IDS, pieceId) && turn === constants.BLACK)) ? true : false;
}

function getCellClasses(row, col, pieceId, turn, isPotentialMove, selected) {
    var clickableClasses = isSelectableCell(pieceId, turn, isPotentialMove) ? 'clickable' : '';
    clickableClasses = isPotentialMove ? 'clickable potential-move' : clickableClasses;
    var selectedClass = selected ? ' selected' : '';

    return 'cell row' + row + ' col' + col + ' ' + getColorClass(row, col) + ' ' + clickableClasses + selectedClass;
}

function getCellContent(row, col, pieceId) {
    if (row === 0) {
        return col === 0 ? '' : col;
    } else if (col === 0) {
        return String.fromCharCode(96 + row);
    } else if (pieceId){
        return _.find(constants.PIECES, {id: pieceId}).repr;
    }
    return '';
}

export default React.createClass({
    displayName: 'Cell',

    propTypes: {
        rowNum: React.PropTypes.number.isRequired,
        colNum: React.PropTypes.number.isRequired,
        pieceId: React.PropTypes.number,
        selectPiece: React.PropTypes.func,
        turn: React.PropTypes.string,
        isPotentialMove: React.PropTypes.bool
    },

    getInitialState: function () {
        return {
            selected: false
        }
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        this.setState({selected: false});
        return !_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState);
    },

    onClick: function () {
        if (this.props.isPotentialMove) {
            this.props.selectMove(this.props.rowNum, this.props.colNum);
        } else if (isSelectableCell(this.props.pieceId, this.props.turn)){
            this.props.selectPiece(this.props.rowNum, this.props.colNum, this.props.pieceId);
            this.setState({selected: true});
        }
    },

    render: function () {
        var cellClass = getCellClasses(this.props.rowNum, this.props.colNum,  this.props.pieceId,
                                        this.props.turn, this.props.isPotentialMove, this.state.selected);
        var cellContent = getCellContent(this.props.rowNum, this.props.colNum, this.props.pieceId);

        return (<div className={cellClass} onClick={this.onClick}>
                    {cellContent}
                </div>);
    }
});