import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main';
import './styles/Board.scss';
import './styles/Main.scss';
import './styles/Cell.scss';

ReactDOM.render(<Main />, document.getElementById('root'));