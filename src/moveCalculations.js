import _ from 'lodash';
import constants from './constants';

function filterOutOfRangeResults(stepsArr) {
    return _.filter(stepsArr, function (step) {
        return step.row !== 0 && step.col !== 0 && step.row < 9 && step.col < 9;
    });
}

function calcDiagonalValidSteps(row, col) {
    var validSteps = [];

    var lowerCol = col;
    var upperCol = col;
    _.forEach(_.range(row - 1, 0), function(rowNum) {
        lowerCol = lowerCol - 1;
        upperCol = upperCol + 1;

        validSteps.push({row: rowNum, col: lowerCol});
        validSteps.push({row: rowNum, col: upperCol});
    });

    lowerCol = col;
    upperCol = col;
    _.forEach(_.range(row + 1, 9), function(rowNum) {
        lowerCol = lowerCol - 1;
        upperCol = upperCol + 1;

        validSteps.push({row: rowNum, col: lowerCol});
        validSteps.push({row: rowNum, col: upperCol});
    });

    return _.filter(validSteps, function (cell) {return cell.col !== 0});
}

function calcVerticalAndHorizontalValidSteps(row, col) {
    var verticalSteps = _.map(_.range(1,9), function (rowNum) {
        return {row: rowNum, col: col}
    });
    var horizontalSteps = _.map(_.range(1,9), function (colNum) {
        return {row: row, col: colNum}
    });
    var validSteps = verticalSteps.concat(horizontalSteps);
    _.remove(validSteps, {row: row, col: col});

    return validSteps;
}

function getKingValidSteps(row, col) {
    var validSteps = [
        {row: row - 1, col: col - 1},
        {row: row - 1, col: col},
        {row: row - 1, col: col + 1},
        {row: row, col: col - 1},
        {row: row, col: col + 1},
        {row: row + 1, col: col - 1},
        {row: row + 1, col: col},
        {row: row + 1, col: col + 1}
    ];
    return filterOutOfRangeResults(validSteps);
}

function getRookValidSteps(row, col) {
    return calcVerticalAndHorizontalValidSteps(row, col);
}

function getBishopValidSteps(row, col) {
    return calcDiagonalValidSteps(row, col);
}

function getQueenValidSteps(row, col) {
    return _.concat(calcVerticalAndHorizontalValidSteps(row, col), calcDiagonalValidSteps(row, col));
}

function getKnightValidSteps(row, col) {
    var validSteps = [
        {row: row - 1, col: col - 2},
        {row: row - 2, col: col - 1},
        {row: row - 1, col: col + 2},
        {row: row - 2, col: col + 1},
        {row: row + 1, col: col - 2},
        {row: row + 2, col: col - 1},
        {row: row + 1, col: col + 2},
        {row: row + 2, col: col + 1}
    ];
    return filterOutOfRangeResults(validSteps);
}


function addStepIfValidForCapture (leftCapturePosition, rightCapturePosition, allowedIds, placement) {
    var validSteps = [];
    var leftPositionPlacement = _.find(placement, leftCapturePosition);
    var rightPositionPlacement = _.find(placement, rightCapturePosition);
    if (leftPositionPlacement && _.includes(allowedIds, leftPositionPlacement.pieceId)) {
        validSteps.push(leftCapturePosition);
    }
    if (rightPositionPlacement && _.includes(allowedIds, rightPositionPlacement.pieceId)) {
        validSteps.push(rightCapturePosition);
    }
    return validSteps;
}

function getPawnValidSteps(row, col, turn, placement) {
    var validSteps = [];
    if (turn === constants.WHITE) {
        validSteps.push({row: row - 1, col: col});
        if (row === 7) {
            validSteps.push({row: row - 2, col: col});
        }
        var leftCapturePosition = {row: row - 1, col: col - 1};
        var rightCapturePosition = {row: row - 1, col: col + 1};

        validSteps = validSteps.concat(addStepIfValidForCapture (leftCapturePosition, rightCapturePosition, constants.BLACK_PIECE_IDS, placement));
    } else {
        validSteps.push({row: row + 1, col: col});
        if (row === 2) {
            validSteps.push({row: row + 2, col: col});
        }

        var leftCapturePosition = {row: row + 1, col: col - 1};
        var rightCapturePosition = {row: row + 1, col: col + 1};

        validSteps = validSteps.concat(addStepIfValidForCapture (leftCapturePosition, rightCapturePosition, constants.WHITE_PIECE_IDS, placement));
    }
    return validSteps;

}

function removeSameColorSteps (validSteps, turn, placement) {
    var sameColorIds = turn === constants.WHITE ? constants.WHITE_PIECE_IDS : constants.BLACK_PIECE_IDS;
    return _.filter(validSteps, function (step) {
        var stepInPlacement = _.find(placement, step);
        return (stepInPlacement && _.includes(sameColorIds, stepInPlacement.pieceId)) ? false : true;
    });
}

function getPotentialMoves(row, col, pieceId, turn, placement) {
    var validSteps;
    switch(pieceId % 6) {
        case 1:
            validSteps = getKingValidSteps(row, col);
            break;
        case 2:
            validSteps = getQueenValidSteps(row, col);
            break;
        case 3:
            validSteps = getRookValidSteps(row, col);
            break;
        case 4:
            validSteps = getBishopValidSteps(row, col);
            break;
        case 5:
            validSteps = getKnightValidSteps(row, col);
            break;
        case 0:
            validSteps = getPawnValidSteps(row, col, turn, placement);
            break;
    }
    return removeSameColorSteps(validSteps, turn, placement);
}

export default {
    getPotentialMoves: getPotentialMoves
};
