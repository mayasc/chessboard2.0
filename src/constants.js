var PIECES = {
    WHITE_KING: {
        id: 1,
        repr: '\u2654'
    },
    WHITE_QUEEN: {
        id: 2,
        repr: '\u2655'
    },
    WHITE_ROOK: {
        id: 3,
        repr: '\u2656'
    },
    WHITE_BISHOP: {
        id: 4,
        repr: '\u2657'
    },
    WHITE_KNIGHT: {
        id: 5,
        repr: '\u2658'
    },
    WHITE_PAWN: {
        id: 6,
        repr: '\u2659'
    },
    BLACK_KING: {
        id: 7,
        repr: '\u265A'
    },
    BLACK_QUEEN: {
        id: 8,
        repr: '\u265B'
    },
    BLACK_ROOK: {
        id: 9,
        repr: '\u265C'
    },
    BLACK_BISHOP: {
        id: 10,
        repr: '\u265D'
    },
    BLACK_KNIGHT: {
        id: 11,
        repr: '\u265E'
    },
    BLACK_PAWN: {
        id: 12,
        repr: '\u265F'
    }
};

export default {
    PIECES: PIECES,
    INITIAL_PLACEMENT: [
        {row: 1, col: 1, pieceId: PIECES.BLACK_ROOK.id},
        {row: 1, col: 2, pieceId: PIECES.BLACK_KNIGHT.id},
        {row: 1, col: 3, pieceId: PIECES.BLACK_BISHOP.id},
        {row: 1, col: 4, pieceId: PIECES.BLACK_QUEEN.id},
        {row: 1, col: 5, pieceId: PIECES.BLACK_KING.id},
        {row: 1, col: 6, pieceId: PIECES.BLACK_BISHOP.id},
        {row: 1, col: 7, pieceId: PIECES.BLACK_KNIGHT.id},
        {row: 1, col: 8, pieceId: PIECES.BLACK_ROOK.id},
        {row: 2, col: 1, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 2, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 3, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 4, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 5, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 6, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 7, pieceId: PIECES.BLACK_PAWN.id},
        {row: 2, col: 8, pieceId: PIECES.BLACK_PAWN.id},
        {row: 8, col: 1, pieceId: PIECES.WHITE_ROOK.id},
        {row: 8, col: 2, pieceId: PIECES.WHITE_KNIGHT.id},
        {row: 8, col: 3, pieceId: PIECES.WHITE_BISHOP.id},
        {row: 8, col: 4, pieceId: PIECES.WHITE_QUEEN.id},
        {row: 8, col: 5, pieceId: PIECES.WHITE_KING.id},
        {row: 8, col: 6, pieceId: PIECES.WHITE_BISHOP.id},
        {row: 8, col: 7, pieceId: PIECES.WHITE_KNIGHT.id},
        {row: 8, col: 8, pieceId: PIECES.WHITE_ROOK.id},
        {row: 7, col: 1, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 2, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 3, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 4, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 5, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 6, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 7, pieceId: PIECES.WHITE_PAWN.id},
        {row: 7, col: 8, pieceId: PIECES.WHITE_PAWN.id}
    ],
    TABLE_ROW: [0, 1, 2, 3, 4, 5, 6, 7, 8],
    TABLE_COLUMNS: [0, 1, 2, 3, 4, 5, 6, 7, 8],
    WHITE_PIECE_IDS: [1, 2, 3, 4, 5, 6],
    BLACK_PIECE_IDS: [7, 8, 9, 10, 11, 12],
    WHITE: 'white',
    BLACK: 'black'
};